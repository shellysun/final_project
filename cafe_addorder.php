<!doctype html>
<?php
require('cafe_functions.php');
require('cafe_values.php');
html_head("Orders");
require('cafe_header.php');
require('cafe_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
<h2>Add Order</h2>
<form action="cafe_addorder.php" method="post">
  <table border="0">
   <tr>
      <td>Name</td>
      <td align="left">
		 <select name="first">
		
<?php
  // Replace text field with a select pull down menu.
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display all types in the types table
    $results = $db->query('SELECT * FROM Orderers');
    foreach($results as $row)
    {
       print "<option value=".$row['first'].">".$row['first']." ".$row['last']."</option>";
    }

    // close the database connection
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
		</select>
      </td>
    </tr>
    <tr>
    
    <tr>
      <td>Type</td>
      <td align="left">
		 <select name="type">
<?php
  // Replace text field with a select pull down menu.
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display all types in the types table
    $result = $db->query('SELECT * FROM drink_types');
    foreach($result as $row)
    {
      print "<option value=".$row['type'].">".$row['type']."</option>";
    }

    // close the database connection
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
	<tr>
      <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
    </tr>
  </table>
</form>
<?php
} else {
  $first = $_POST['first'];
    $last = $_POST['last'];
  $type = $_POST['type'];
 

  //clean up and validate data
  $first = trim($first);
  $last = trim($last);
  $type = trim($type);
  

  $errors = validate_cafe($first, $last, $type);
  if (empty($errors)) {
    try {
      //open the database
      $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
      //insert data...
  $db->exec("INSERT INTO Drink (type ) VALUES ('$type');");
    
      //get the last id value inserted into the table
      $last_id = $db->lastInsertId();
   
      //now output the data from the insert to a simple html table...
      print "<h2>Orders Added</h2>";
      print "<table border=1>";
      print "<tr>";
    print "<td>first name</td><td>last name</td><td>Type</td>";
      print "</tr>";
	  
	 
     $row = $db->query("SELECT * FROM drink_types, Orderers ")->fetch(PDO::FETCH_ASSOC);
	 
	  print "<tr>";
      print "<td>".$row['first']."</td>";
	  print "<td>".$row['last']."</td>";
      print "<td>".$row['type']."</td>";
     
      print "</tr>";
      print "</table>";

      // close the database connection
      $db = NULL;
    }

    catch(PDOException $e){
      echo 'Exception : '.$e->getMessage();
      echo "<br/>";
      $db = NULL;
    }
  } else {
    echo "Errors found in order entry:<br/>";
    foreach($errors as $error) {
      echo " -  $error <br/>";
    }
    try_again("Please reenter.<br/>");
  }
}
require('cafe_footer.php');
?>
