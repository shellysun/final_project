<?php
require('cafe_values.php');
# CSCI59P standard functions
function html_head($title) {
  echo '<html lang="en">';
  echo '<head>';
  echo '<meta charset="utf-8">';
  echo "<title>$title</title>";
  echo '<link rel="stylesheet" href="cafe.css">';
  echo '</head>';
  echo '<body>';
}


function try_again($str) {
  echo $str;
  echo "<br/>";
  //the following emulates pressing the back button on a browser
  echo '<a href="#" onclick="history.back(); return false;">Try Again</a>';
  require('cafe_footer.php');
  exit;
  require('cafe_footer.php');
}

function validate_cafe($first, $last, $type ) {
  $error_messages = array(); # Create empty error_messages array.
 
  try {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        
    //check for duplicate  name
     if ( strlen($first) != 0 ) {
      $sql = "SELECT COUNT(*) FROM Orderers WHERE first = '$first'";
      $result = $db->query($sql)->fetch(); //count the number of entries with the drink name
      if ( $result[0] == 0) {
        array_push($error_messages, " first $first is not defined. first must be valid.");
      }
    }

	
  }
catch(PDOException $e){
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }

  return $error_messages;
}
  
function validate_name($first, $last) {
  $error_messages = array(); # Create empty error_messages array.
   if ( strlen($first) == 0 ) {
    array_push($error_messages, "Please enter a first name.");
  }
 
  if ( strlen($last) == 0 ) {
    array_push($error_messages, "Please enter last name.");
  }
  try {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	 //check for duplicate  name
    if ( strlen($first) != 0 ) {
       $sql = "SELECT COUNT(*) FROM Orderers WHERE first = '$first'";
      $result = $db->query($sql)->fetch(); //count the number of entries with the  name
      if ( $result[0] > 0) {
        array_push($error_messages, "$first is not unique. first name names must be unique.");
      }
    }
	
	 
	
  }


  catch(PDOException $e){
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }

  return $error_messages;
}
function validate_drink($drink, $type) {
  $error_messages = array(); # Create empty error_messages array.
   if ( strlen($drink) == 0 ) {
    array_push($error_messages, "Please enter a drink name.");
  }
 
  if ( strlen($type) == 0 ) {
    array_push($error_messages, "Type field must have a type.");
  }
  try {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	 //check for duplicate  name
    if ( strlen($drink) != 0 ) {
       $sql = "SELECT COUNT(*) FROM Drink WHERE drink = '$drink'";
      $result = $db->query($sql)->fetch(); //count the number of entries with the  name
      if ( $result[0] > 0) {
        array_push($error_messages, "$drink is not unique. drink name names must be unique.");
      }
    }
	
	 //check for invalid name type
    if ( strlen($type) != 0 ) {
    $sql = "SELECT COUNT(*) FROM drink_types WHERE type = '$type'";
      $result = $db->query($sql)->fetch(); //count the number of entries with the drink name
      if ( $result[0] == 0) {
        array_push($error_messages, " type $type is not defined. Type must be valid.");
      }
    }
	
  }


  catch(PDOException $e){
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }

  return $error_messages;
}
// Validate the date format as YYYY-MM-DD
function MyCheckDate( $postedDate ) {
   if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $postedDate, $datebit)) {
      return checkdate($datebit[2] , $datebit[3] , $datebit[1]);
   } else {
      return false;
   }
}

?>
