<!doctype html>
<?php
require('cafe_functions.php');
html_head("cafe_alreadyordered");
require('cafe_header.php');
require('cafe_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    //get date one week in advance
    $result = $db->query("SELECT CURDATE() + INTERVAL 1 WEEK")->fetch();
    $next_week = $result[0];
?>

    <h2>select already ordered drink</h2>
    <form action="cafe_alreadyordered.php" method="post">
      <!-- select user checking out media -->
      Checked out to:
      <select name="user">
        	
<?php
    //display all users in the mlib_users  table
    $result = $db->query('SELECT * FROM Orderers ');
    foreach($result as $row)
    {
      print "<option value=".$row['id'].">".$row['first']."</option>";
    }
?>

      </select><br/>
      <!-- prompt for date to return equipment. Default is one week from today -->
      Reserve Till (yyyy-mm-dd):
      <?php print "<input type 'text' name='date_in' value='$next_week' /><br/>";?>
      <!-- display media that can be checked out -->
      <table border=1>
        <tr>
        	<td>Click to Reserve</td><td>Title</td><td>Type</td><td>Description</td>
        </tr>
        
<?php
    $result = $db->query("SELECT * FROM drink_types WHERE status = 'active' ");
    foreach($result as $row)
    {
      print "<tr>";
      print "<td><input type='checkbox' name='id[]' value=".$row['id']."></td>";
      
      print "<td>".$row['type']."</td>";
 
      print "</tr>";
    }
?>
      </table>
      <input type="submit" name="submit" value = "Submit"/><br/>
    </form>

<?php
    
    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }


} else {
?>

    <h2>Media Reserved</h2>

<?php
  $id = $_POST['id'];
  $user = $_POST['user'];
  $date_in = $_POST['date_in'];

  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    //get the name for the user
    $result = $db->query("SELECT * FROM Orderers where id = $user")->fetch();
    $user_name = $result['first']." ".$result['last'];
    
    //get todays date
    $result = $db->query("SELECT CURDATE()")->fetch();
    $today = $result[0];

    //validate date_in
    if (!MyCheckDate($date_in)) {
      try_again("Please enter an valid date. Format is yyyy-mm-dd.");
    }
    //check for date in the past
    if ($date_in < $today) {
      try_again("The Reserve Till date is earlier than today: ".$today);
    }


    $n = count($id);
    if ($n == 0) {
      echo "You did not select any items to reserve.<br/>";
    } else {
      //update each piece of equipment with user_id(user), date_in
      for($i=0; $i < $n; $i++)
      {
        $db->exec("UPDATE Drink SET user_id = $user, date_in = '$date_in' WHERE id = $id[$i]");
      }
      
      //now output the data to a simple html table...
      print "<table border=1>";
      print "<tr>";
      print "<td>Title</td><td>Type</td><td>Description</td><td>User</td><td>Reserved Till</td>";
      print "</tr>";
      for($i=0; $i < $n; $i++)
      {
        $sql = "SELECT * FROM Drink where id = $id[$i]";
        $row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        print "<tr>";
        print "<td>".$row['title']."</td>";
        print "<td>".$row['type']."</td>";
        print "<td>".$row['description']."</td>";
        print "<td>".$user_name."</td>";
        print "<td>".$row['date_in']."</td>";
        print "</tr>";
      }
      print "</table>";
    }

    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }
}
require('mlib_footer.php');
?>
