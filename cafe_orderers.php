<!doctype html>
<?php
require('cafe_functions.php');
require('cafe_values.php');
html_head("Orders");
require('cafe_header.php');
require('cafe_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
<h2>Orderer</h2>
<form action="cafe_orderers.php" method="post">
  <table border="0">
      <tr>
      <td>First name</td>
      <td align="left"><input type="text" name="first" size="35" maxlength="35"></td>
    </tr>
	
	  <tr>
      <td>Last name</td>
      <td align="left"><input type="text" name="last" size="35" maxlength="35"></td>
    </tr>
	

		
    
    <tr>
      <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
    </tr>
  </table>
</form>
<?php
} else {
	$first= $_POST['first'];
    $last = $_POST['last'];
  

  //clean up and validate data
  $first = trim($first);
   $last = trim($last);
 

  $errors = validate_name($first, $last);
  if (empty($errors)) {
    try {
      //open the database
      $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
      //insert data...
      $db->exec("INSERT INTO Orderers (first, last) VALUES ('$first', '$last');");
    
      //get the last id value inserted into the table
      $last_id = $db->lastInsertId();
   
      //now output the data from the insert to a simple html table...
      print "<h2>Orders added</h2>";
      print "<table border=1>";
      print "<tr>";
     print "<td>First name</td><td>Last name</td>";
      print "</tr>";
       $row = $db->query("SELECT * FROM Orderers where id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
      print "<tr>";
	  print "<td>".$row['first']."</td>";
      print "<td>".$row['last']."</td>";
           
      print "</tr>";
      print "</table>";

      // close the database connection
      $db = NULL;
    }

    catch(PDOException $e){
      echo 'Exception : '.$e->getMessage();
      echo "<br/>";
      $db = NULL;
    }
  } else {
    echo "Errors found in orderer entry:<br/>";
    foreach($errors as $error) {
      echo " -  $error <br/>";
    }
    try_again("Please reenter.<br/>");
  }
}
require('cafe_footer.php');
?>
