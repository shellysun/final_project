<!doctype html>
<?php
require('cafe_functions.php');
require('cafe_values.php');
html_head("add drinks");
require('cafe_header.php');
require('cafe_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
<h2>Order drinks</h2>
<form action="cafe_adddrinks.php" method="post">
  <table border="0">
      <tr>
      <td>Drink</td>
      <td align="left"><input type="text" name="drink" size="35" maxlength="35"></td>
    </tr>
	   <tr>
      <td>Type</td>
      <td align="left">
		 <select name="type">
<?php
  // Replace text field with a select pull down menu.
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display all types in the types table
    $result = $db->query('SELECT * FROM drink_types');
	
	
    foreach($result as $row)
    {
      print "<option value=".$row['type'].">".$row['type']."</option>";
    }
    // close the database connection
    $db = NULL;
  }

  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
		</select>
      </td>
    </tr>
    
    <tr>
      <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
    </tr>
  </table>
</form>
<?php
} else {
	$drink = $_POST['drink'];
    $type = $_POST['type'];
  

  //clean up and validate data
  $drink = trim($drink);
   $type = trim($type);
 

  $errors = validate_drink( $drink, $type);
  if (empty($errors)) {
    try {
      //open the database
      $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
      //insert data...
     $db->exec("INSERT INTO Drink (drink ) VALUES ('$drink');");
    
      //get the last id value inserted into the table
      $last_id = $db->lastInsertId();
   
      //now output the data from the insert to a simple html table...
      print "<h2>Drinks added</h2>";
      print "<table border=1>";
      print "<tr>";
     print "<td>Drink</td><td>Type</td>";
      print "</tr>";
     $row = $db->query("SELECT * FROM Drink where drink = '$drink'")->fetch(PDO::FETCH_ASSOC);
	 $row2 = $db->query("SELECT * FROM drink_types where status = 'active'")->fetch(PDO::FETCH_ASSOC);
      print "<tr>";
	  print "<td>".$row['drink']."</td>";
      print "<td>".$row2['type']."</td>";
           
      print "</tr>";
      print "</table>";

      // close the database connection
      $db = NULL;
    }

    catch(PDOException $e){
      echo 'Exception : '.$e->getMessage();
      echo "<br/>";
      $db = NULL;
    }
  } else {
    echo "Errors found in drink entry:<br/>";
    foreach($errors as $error) {
      echo " -  $error <br/>";
    }
    try_again("Please reenter.<br/>");
  }
}
require('cafe_footer.php');
?>
