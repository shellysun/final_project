<!doctype html>
<?php
require('cafe_functions.php');
html_head("cancel drink");
require('cafe_header.php');
require('cafe_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>

    <h2>cancel</h2>
    <form action="cafe_cancel.php" method="post">
      <!-- display checked out of medias -->
      <table border=1>
        <tr>
        	<td>Click to cancel</td><td>Drink</td><td>User</td><td>Reserved Till</td>
        </tr>
        
<?php
    $result = $db->query("SELECT * FROM Drink WHERE status = 'active' AND user_id > 0 ORDER by drink");
    foreach($result as $row)
    {
      print "<tr>";
      print "<td><input type='checkbox' name='id[]' value=".$row['id']."></td>";
      print "<td>".$row['drink']."</td>";
      
      $user_id = $row['user_id'];
      $result = $db->query("SELECT * FROM Orderers WHERE id = $user_id")->fetch();
      $user_name = $result['first']." ".$result['last'];
      print "<td>".$user_name."</td>";
      print "<td>".$row['date_in']."</td>";
      print "</tr>";
    }
?>
      </table>
      <input type="submit" name="submit" value = "Submit"/><br/>
    </form>

<?php
    
    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }


} else {
?>

    <h2>Drink Canceled</h2>

<?php
  $id = $_POST['id'];

  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $n = count($id);
    if ($n == 0) {
      echo "Please select items to cancel.<br/>";
    } else {
      //update each piece of media with user_id = 0
      for($i=0; $i < $n; $i++)
      {
        $db->exec("UPDATE Drink SET user_id = 0 WHERE id = $id[$i]");
      }

      //now output the data to a simple html table...
      print "<table border=1>";
      print "<tr>";
      print "<td>Title</td>";
      print "</tr>";
      for($i=0; $i < $n; $i++)
      {
        $sql = "SELECT * FROM Drink WHERE id = $id[$i]";
        $row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        print "<tr>";
        print "<td>".$row['drink']."</td>";
      
        print "</tr>";
      }
      print "</table>";
    }

    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }
}
require('cafe_footer.php');
?>
