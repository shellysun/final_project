<!doctype html>
<?php
require('cafe_functions.php');
html_head("drink status");
require('cafe_header.php');
require('cafe_sidebar.php');
require('cafe_values.php');


# Code for your web page follows.
try
{
  //open the database
  $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>

<h2>Drink  Status</h2>
<!-- display all media -->
<table border=1>
  <tr>
 <td>Drink </td><td>User</td><td>Date</td>
	
  </tr>
<?php
  
 $query = "SELECT * FROM Drink";
 // $query = "SELECT * FROM Drink WHERE status = 'active'";

   
  $result = $db->query($query);
  foreach($result as $row) {
    print "<tr>";
   	print "<td>".$row['drink']."</td>";
    $user_id = $row['user_id'];
	if ($user_id > 0) {
		$result = $db->query("SELECT * FROM Orderers where id= $user_id")->fetch();
		$user_name = $result['first']." ".$result['last'];
		$date_in = $row['date_in'];
	} else {
		$user_name = "available";
		$date_in = "not reserved";
	}
	print "<td>".$user_name."</td>";
    print "<td>".$date_in."</td>";
 
    print "</tr>";
  }

  print "</table>";

  // close the database connection
  $db = NULL;
}
catch(PDOException $e)
{
  echo 'Exception : '.$e->getMessage();
  echo "<br/>";
  $db = NULL;
}
require('cafe_footer.php');
?>
